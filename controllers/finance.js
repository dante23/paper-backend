const Models = require("../models/index")
const FinanceTransaction = Models.finance_transaction
const Op = Models.Sequelize.Op

const getFinances = async (req, res) => {
    const { sort, order, page } = req.body
    const currentPage = page || 1
    const perPage = 10
    const attributes = ['id', 'transaction_time', 'amount', 'reference', 'created_at'];
    const offset = (currentPage - 1) * perPage
    const where = {
        'soft_delete': {
            [Op.is]: null
        }
    }

    // select it
    await FinanceTransaction.findAndCountAll({
        where,
        attributes,
        offset: offset,
        limit: perPage,
        order: [[order, sort]]
    }).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });

}

const getFinance = async (req, res) => {
    const attributes = ['id', 'transaction_time', 'amount', 'reference', 'created_at'];
    const { id } = req.params
    const where = {
        id,
        'soft_delete': {
            [Op.is]: null
        }
    }

    // select it
    await FinanceTransaction.findOne({ where, attributes }).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });
}

const createFinance = async (req, res) => {
    // destructure params
    const { amount, reference, transaction_time, user_id, finance_type_id } = req.body

    if (amount != null || reference != null) {

        //set into variables
        const attributes = { amount, reference, user_id, finance_type_id };
        attributes.transaction_time = transaction_time != null ? transaction_time : new Date()
        attributes.created_at = new Date()

        //create it
        await FinanceTransaction.create(attributes).then((result) => {
            res.json(result);
        }).catch((err) => {
            res.status(400).json({ 'code': 400, 'messages': err })
        });
    } else {
        res.status(400).send('params malformed')
    }

}

const updateFinance = async (req, res) => {
    // destructure params
    const { id } = req.params
    const { amount, reference, finance_type_id } = req.body

    if (amount != null || reference != null) {
        //set into variables
        const attributes = { amount, reference, finance_type_id };
        attributes.updated_at = new Date()
        const where = { where: { id } }

        //update it
        await FinanceTransaction.update(attributes, where).then((result) => {
            res.json(result);
        }).catch((err) => {
            res.status(400).json({ 'code': 400, 'messages': err })
        });
    }
    res.status(400).send('params malformed')
}

const deleteFinance = async (req, res) => {
    // destructure params
    const { id } = req.params

    //set into variables
    const where = { where: { id } }
    const attributes = { "soft_delete": new Date() }

    //update it
    await FinanceTransaction.update(attributes, where).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });
}

module.exports = { getFinances, getFinance, createFinance, updateFinance, deleteFinance }