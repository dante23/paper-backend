const Models = require("../models/index")
const Users = Models.users
let bcrypt = require("bcrypt")
let salt = bcrypt.genSaltSync(10)

const getUsers = async (req, res) => {
    const { sort, order, page } = req.body
    const currentPage = page || 1
    const perPage = 10
    const attributes = ['id', 'email', 'username'];
    const offset = ( currentPage - 1 ) * perPage

    // select it
    await Users.findAndCountAll({ 
        attributes,
        offset: offset, 
        limit: perPage, 
        order: [[order, sort]] }).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });

}

const getUser = async (req, res) => {
    const attributes = ['id', 'email', 'username'];
    const { id } = req.params
    const where = { id }

    // select it
    await Users.findOne({ where, attributes }).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });
}

const registerUser = async (req, res) => {
    // destructure params
    const { username, email, password } = req.body

    if ((username != null || email != null) && password != null) {
        const passwordHash = bcrypt.hashSync(password, salt)
        //set into variables
        const attributes = { email, username, password: passwordHash };

        //insert it
        await Users.create(attributes).then((result) => {
            res.json(result);
        }).catch((err) => {
            res.status(400).json({ 'code': 400, 'messages': err })
        });
    }
    res.status(400).send('params malformed')

}

const updateUser = async (req, res) => {
    // destructure params
    const { id } = req.params
    const { username, email } = req.body

    //set into variables
    const attributes = { email, username };
    const where = { where: { id } }

    //update it
    await Users.update(attributes, where).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });
}

const deleteUser = async (req, res) => {
    // destructure params
    const { id } = req.params

    //set into variables
    const where = { where: { id } }

    //update it
    await Users.destroy(where).then((result) => {
        res.json(result);
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });
}

module.exports = { getUsers, getUser, registerUser, updateUser, deleteUser }