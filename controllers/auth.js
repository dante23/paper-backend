const Models = require("../models/index")
const Users = Models.users
const UserAuth = Models.user_auth
let bcrypt = require("bcrypt")
let salt = bcrypt.genSaltSync(10)
const crypto = require("crypto")
const authMiddleware = require("../middlewares/auth")

const currentUser = async (req, res) => {
    const userDb = await Users.findOne({ where: { id: req.user.id }, attributes: ['id', 'email', 'username'] })
    req.user = {
        id: userDb.id,
        username: userDb.username,
        email: userDb.email,
    }
    res.json(req.user)
}

const changePassword = async (req, res) => {
    // destructure params
    const { id } = req.params
    const { password } = req.body
    // hashing password
    const passwordHash = bcrypt.hashSync(password, salt)

    // set variables for db
    const attributes = { password: passwordHash }
    const where = { where: { id } }

    // update it
    await Users.update(attributes, where).then((result) => {
        res.json({ 'code': 200, data: result })
    }).catch((err) => {
        res.status(400).json({ 'code': 400, 'messages': err })
    });
}

const login = async (req, res) => {
    try {
        // destructure params
        const { username, email, password } = req.body

        let where = ''
        if (username != null) {
            where = { username }
        } else {
            where = { email }
        }
        //retrieve user old
        let usr = ''
        await Users.findOne({ where, attributes: ['id', 'username', 'password', 'email', 'created_at'] })
            .then((result) => {
                usr = result
            }).catch((err) => {
                console.log(err)
                //res.status(401).send('email/username is wrong').toString()
            });
        if (usr) {
            if (checkPassword(password, usr.password)) {
                usr.dataValues.token = authMiddleware.generateAccessToken(username != null ? username : email)
                //retrieve old auth
                let oldAuth = ''
                await UserAuth.findOne({ where: { user_id: usr.id }, attributes: ['user_id', 'token'] })
                    .then((result) => {
                        oldAuth = result
                    }).catch((err) => {
                        console.log(err)
                    });

                //console.log('OLDAUTH', oldAuth)
                //check if old auth there
                if (oldAuth !== null) {
                    UserAuth.update({ user_id: usr.id, token: usr.dataValues.token }, {
                        where: { user_id: oldAuth.user_id }
                    })
                } else {
                    UserAuth.create({ user_id: usr.id, token: usr.dataValues.token })
                }
                res.json(usr)
            } else {
                res.status(401).send('password incorrect').toString()
            }
        } else {
            let u = username != null ? 'username' : 'email'
            res.status(401).send(`${u} is wrong`)
        }

    } catch (error) {
        console.log(error)
    }

}

const logout = async (req, res) => {
    const { user_id } = req.body
    await UserAuth.destroy({ where: { user_id } }).then((r) => {
        //console.log(r)
        res.json(r)
    }).catch((err) => {
        //console.log(err)
        res.status(401).send("cant get user id or something else")
    });
     
}

const checkPassword = async (pass, userPass) => {
    return bcrypt.compareSync(pass, userPass) ? true : false;
}

const generateToken = async (req, res) => {
    res.send(crypto.randomBytes(64).toString('hex'))
}

module.exports = {
    currentUser,
    changePassword,
    login,
    logout,
    checkPassword,
    generateToken,
}