const jwt = require("jsonwebtoken")
const Models = require("../models/index")
const UserAuth = Models.user_auth
const Users = Models.users

const authCheck = async (req, res, next) => {

    try {
        const { authtoken } = req.headers

        if (verifyToken(authtoken)) {
            const auth = await UserAuth.findOne({ where: { token: authtoken } })
            console.log("userauth", auth.user_id)
            if (auth) {
                req.user = { id: auth.user_id }
                next()

            } else {
                res.status(401).json({ 'code': 401, 'messages': 'Invalid Or Expired Token ' })
            }
        }

    } catch (error) {
        console.log('AUTH CHECK ERROR', error)
        res.status(401).json({ 'code': 401, 'messages': 'Invalid Or Expired Token ' })
    }
}

const generateAccessToken = (user) => {
    let getJwt = jwt.sign({ data: user }, process.env.TOKEN_SECRET, { expiresIn: "1d" })
    return getJwt
}

const verifyToken = (token) => {
    let checkToken = jwt.verify(token, process.env.TOKEN_SECRET)
    return checkToken
}

const checkExistUser = async (email = null, username = null) => {
    let where = ''
    let userId = ''
    if (username !== null) {
        where = { username }
    } else if (email !== null) {
        where = { email }
    }
    // get id from users
    await Users.findOne({ where, attributes: ['id'] }).then((result) => userId = result).catch((err) => console.log(err));

    return userId
}

const checkExistTokenUser = async (id) => {
    //retrieve data in db
    let userDb = '';
    where = { id }
    const attributesAuth = ['token'];

    await UserAuth.findOne({ where, attributes: attributesAuth }).then((result) => userDb = result).catch((err) => console.log(err));
    console.log('token user ', userDb)
    return userDb
}

const getUserByToken = async (token) => {
    let userId, userDb

    await UserAuth.findOne({ where: { token }, attributes: ['id'] }).then(u => userId = u.id).catch(e => console.log('error get id from token', e))
    await User.findOne({ where: { id: userId }, attributes: ['id', 'username', 'email'] }).then(u => userDb = u).catch(e => console.log('error get information of user', e))

    return userDb
}

module.exports = {
    authCheck,
    checkExistTokenUser,
    generateAccessToken,
    checkExistUser,
    getUserByToken,
    verifyToken
}