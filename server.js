const express = require("express")
const { Sequelize } = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config.json')[env];
const morgan = require("morgan")
const bodyParser = require("body-parser")
const cors = require("cors")
require('dotenv').config()
const { readdirSync } = require("fs")

// app

const app = express()

// db
// const db = require("./Models")
// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
//   })

// db
// Option 2: Passing parameters separately (other dialects)
const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect
});

( async () => {
    await sequelize.authenticate()
    .then(() => console.log('Connection has been established successfully.'))
    .catch((e) => console.log(`can't connect into db`, e))
    sequelize.close()
})()


//middleware
app.use(morgan(process.env.NODE_ENV))
app.use(bodyParser.json({ limit: "2mb" }))
app.use(cors())

//routes autoloading
const prefix = "/api";
readdirSync("./routes").map( (r) => app.use( prefix, require("./routes/" + r) ) );

// running apps
const port = process.env.PORT || 3003
app.listen(port, () => console.log(`running on ${port}`))