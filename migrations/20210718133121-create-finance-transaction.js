'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('finance_transaction', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER, 
        references: {
          model: {
            tableName: 'users',
            //schema: 'schema'
          },
          key: 'id'
        },
        allowNull: false,
      },
      finance_account_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'finance_type',
            //schema: 'schema'
          },
          key: 'id'
        },
        allowNull: false,
      },
      transaction_time: {
        type: Sequelize.DATE
      },
      amount: {
        type: Sequelize.DECIMAL(10, 2)
      },
      reference: {
        type: Sequelize.STRING(191)
      },
      soft_delete: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('finance_transaction');
  }
};