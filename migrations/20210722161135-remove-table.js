'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */

    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('user_auth', 'updated_at', { transaction: t }),
        queryInterface.removeColumn('finance_type', 'updated_at', { transaction: t })
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('user_auth', 'updated_at', {
          type: Sequelize.DataTypes.DATE}, {transaction: t}),
        queryInterface.addColumn('finance_type', 'updated_at', {
          type: Sequelize.DataTypes.DATE}, {transaction: t})
      ]);
    });
  }
};
