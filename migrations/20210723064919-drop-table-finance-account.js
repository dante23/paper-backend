'use strict';

const { query } = require("express");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return queryInterface.dropTable('finance_account');
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.createTable('finance_account', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'users',
            //schema: 'schema'
          },
          key: 'id'
        },
        allowNull: false,
        onDelete: "CASCADE"
      },
      name: {
        type: Sequelize.STRING(191)
      },
      amount: {
        type: Sequelize.DECIMAL(10, 2)
      },
      description: {
        type: Sequelize.STRING(191)
      },
      finance_type_id: {
        type: Sequelize.INTEGER,
        references: {
          model: {
            tableName: 'finance_type',
            //schema: 'schema'
          },
          key: 'id'
        },
        allowNull: false,
      },
      soft_delete: {
        type: Sequelize.DATE
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  }
};
