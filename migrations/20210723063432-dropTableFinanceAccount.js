'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('finance_transaction', 'finance_account_id', { transaction: t })
      ])
    })
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('finance_transaction', 'finance_account_id', {
          type: Sequelize.DataTypes.INTEGER,
          references: {
            model: {
              tableName: 'finance_type',
              //schema: 'schema'
            },
            key: 'id'
          },
          allowNull: false,
        }, { transaction: t })
      ])
    })
  }
};
