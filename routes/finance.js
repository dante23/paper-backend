const express = require("express");

const routes = express.Router();

const { authCheck } = require("../middlewares/auth");
const { 
    getFinances, 
    getFinance, 
    createFinance, 
    updateFinance, 
    deleteFinance } = require("../controllers/finance");

routes.get('/finances', authCheck, getFinances);
routes.get('/finance/:id', authCheck, getFinance);
routes.post('/finance', authCheck, createFinance);
routes.put('/finance/:id', authCheck, updateFinance);
routes.delete('/finance/:id', authCheck, deleteFinance);

module.exports = routes;