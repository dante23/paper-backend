const express = require("express")
const routes = express.Router()

const { authCheck } = require("../middlewares/auth")
const { 
    currentUser, 
    generateToken, 
    login, 
    logout } = require("../controllers/auth")

routes.post("/login", login)
routes.post("/logout", logout)
routes.post("/current-user",authCheck, currentUser)
routes.post("/generate-token", generateToken)

module.exports = routes