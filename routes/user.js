const express = require("express");

const routes = express.Router();

const { authCheck } = require("../middlewares/auth");
const { getUsers, getUser, registerUser, updateUser, deleteUser } = require("../controllers/user");

routes.get('/users', authCheck, getUsers);
routes.get('/user/:id', authCheck, getUser);
routes.post('/user', registerUser);
routes.put('/user/:id', authCheck, updateUser);
routes.delete('/user/:id', authCheck, deleteUser);

module.exports = routes;