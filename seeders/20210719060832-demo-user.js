'use strict';
let faker = require("faker")
let bcrypt = require("bcrypt")
let salt = bcrypt.genSaltSync(10)

const dataUsers = [...Array(100)].map((u) => (
  {
    username: `${faker.random.words(1)}${faker.random.words(1)}`,
    email: `${faker.random.words(1)}@${faker.random.words(1)}.com`,
    password: bcrypt.hashSync(faker.lorem.sentences(8), salt),
    created_at: new Date(),
    updated_at: new Date()
  }
))

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('users', dataUsers, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
  }
};
