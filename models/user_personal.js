'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_personal extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `Models/index` file will call this method automatically.
     */
    static associate(Models) {
      // define association here
    }
  };
  user_personal.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey:true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER, 
      references: {
        model: {
          tableName: 'users',
          //schema: 'schema'
        },
        key: 'id'
      },
      allowNull: false,
      onDelete: 'CASCADE',
    },
    first_name: DataTypes.STRING(191),
    last_name: DataTypes.STRING(191),
    soft_delete: {type: DataTypes.DATE}
  }, {
    sequelize,
    tableName: 'user_personal',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes:[{
      unique: true, fields:['id']
    }]
  });
  return user_personal;
};