'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_auth extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `Models/index` file will call this method automatically.
     */
    static associate(Models) {
      // define association here
    }
  };
  user_auth.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey:true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER, 
      references: {
        model: {
          tableName: 'users',
          //schema: 'schema'
        },
        key: 'id'
      },
      allowNull: false,
      onDelete: 'CASCADE',
    },
    token: DataTypes.STRING(191)
  }, {
    sequelize,
    //freezeTableName: true,
    //underscored: true,
    //modelName: 'user_auth',
    tableName: 'user_auth',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: false,
    indexes: [{
      unique: true, fields:['id', 'token']
    }]
  });
  return user_auth;
};