'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `Models/index` file will call this method automatically.
     */
    static associate(Models) {
      // define association here
    }
  };

  Users.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey:true,
      type: DataTypes.INTEGER
    },
    username: DataTypes.STRING(191),
    email: {
      type: DataTypes.STRING(191),
      validate: {
        isEmail: {
          args: true,
          msg: 'must be email format foo@mail.com'
        },
      }
    },
    password: DataTypes.STRING(191),
    soft_delete: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'users',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  });
  return Users;
};