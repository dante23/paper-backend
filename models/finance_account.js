'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class finance_account extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `Models/index` file will call this method automatically.
     */
    static associate(Models) {
      // define association here
    }
  };
  finance_account.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER, 
      references: {
        model: {
          tableName: 'users',
          //schema: 'schema'
        },
        key: 'id'
      },
      allowNull: false,
      onDelete: 'CASCADE',
    },
    name: DataTypes.STRING(90),
    amount: DataTypes.DECIMAL(10, 2),
    description: DataTypes.STRING(191),
    finance_type_id: DataTypes.INTEGER(11),
    soft_delete: {type: DataTypes.DATE}
  }, {
    sequelize,
    tableName: 'finance_account',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes: [{ unique: true, fields: ['id'] }]
  });
  return finance_account;
};