'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class finance_transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `Models/index` file will call this method automatically.
     */
    static associate(Models) {
      // define association here
    }
  };
  finance_transaction.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    user_id: {
      type: DataTypes.INTEGER, 
      references: {
        model: {
          tableName: 'users',
          //schema: 'schema'
        },
        key: 'id'
      },
      allowNull: false,
      onDelete: 'CASCADE',
    },
    finance_type_id: {
      type: DataTypes.INTEGER,
      references: {
        tableName: 'finance_type',
        key: 'id'
      },
      allowNull: false
    },
    transaction_time: DataTypes.DATE,
    amount: DataTypes.DECIMAL(10, 2),
    reference: DataTypes.STRING(191),
    soft_delete: { type: DataTypes.DATE }
  }, {
    sequelize,
    tableName: 'finance_transaction',
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    indexes: [{ unique: true, fields: ['id'] }]
  });
  return finance_transaction;
};