'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class finance_type extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `Models/index` file will call this method automatically.
     */
    static associate(Models) {
      // define association here
    }
  };
  finance_type.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING(191),
    soft_delete: { type: DataTypes.DATE }
  }, {
    sequelize,
    tableName: 'finance_type',
    timestamps: false,
    createdAt: false,
    updatedAt: false,
    indexes: [{ unique: true, fields: ['id'] }]
  });
  return finance_type;
};