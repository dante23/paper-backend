
# First Step: 
```bash
npm install -> its will install the depencies we are needed
```

generate db with sequelize
- create db -> type: npx sequelize-cli db:create
- migrate db -> type: npx sequelize-cli db:migrate

run: npm run dev (for dev runnning)

api has prefix -> http:domain.com/api/*

# ROUTE
## AUTH
```bash
GET /login
    params: username/email, password
    return: user with token
    {
        "id": 102,
        "username": "123UserName",
        "password": "$2b$10$vUeAj.kstp6lqmvCxtkk/uOnP20Xi9liLfS7oYbU4UDcotksm/vTa",
        "email": "dedif15@gmail.com",
        "created_at": "2021-07-19T08:29:51.000Z",
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiZGVkaWYxNUBnbWFpbC5jb20iLCJpYXQiOjE2MjY5MzkxOTUsImV4cCI6MTYyNzAyNTU5NX0.-WkSprAocXIVmvVMNpLwuoBvxlhgjviZ3KQH5xsKd60"
    }

GET /logout
    params: id
    return: 1 if success then 0 if nothing happened

POST /current-user
    params: headers: authtoken
    return: user current login
    {
        "id": 102,
        "username": "123UserName",
        "email": "dedif15@gmail.com"
    }

POST /generate-token
    return: secret-key for generate JWT token
```
## USERS

```bash
GET /users
    params: sort, order, page, headers: authtoken
    return: lists of user from table users
    [
        {
            "id": 194,
            "email": "mission-critical@Granite.com",
            "username": "attitude-orientedGrocery"
        },
        {
            "id": 192,
            "email": "Republic@Flats.com",
            "username": "Saladfuchsia"
        },
        {
            "id": 193,
            "email": "Small@panel.com",
            "username": "Refineddeposit"
        }
    ]

GET /user/:id
    params: id, headers: authtoken
    return: user
    {
        "id": 105,
        "email": "Data@lime.com",
        "username": "GlovesProfit-focused"
    }         

POST /user -> register user
    params: username, email, password
    return: user had registered
    {
        "id": 204,
        "email": "dedif13@gyahoo.co.id",
        "username": "123UserName",
        "password": "$2b$10$bSw8VcK3PalqHLtKACCTYe68IbfwjlU3MtVhGrx9LDozBdRnVt1Q2",
        "updated_at": "2021-07-22T08:18:34.121Z",
        "created_at": "2021-07-22T08:18:34.121Z"
    }

PUT /user/:id 
    params: id, username, email, headers: authtoken
    return: 1 if affected then 0 if nothing happened

DELETE /user/:id
    params: id, headers: authtoken
    return: 1 if affected then 0 if nothing happened
```

## FINANCE
```bash
GET /finances -> show where soft_delete is null
    params: sort, order, page, headers: authtoken
    return: lists of finance_transaction from table finance_transaction
    {
        "count": 3,
        "rows": [
            {
                "id": 8,
                "transaction_time": "2021-07-22T23:30:02.000Z",
                "amount": "387500.75",
                "reference": "hasil jualan module",
                "created_at": "2021-07-23T08:16:09.000Z"
            },
            {
                "id": 7,
                "transaction_time": "2021-07-22T23:30:02.000Z",
                "amount": "187500.70",
                "reference": "hasil jualan remote",
                "created_at": "2021-07-23T08:13:47.000Z"
            },
            {
                "id": 6,
                "transaction_time": "2021-07-22T23:30:02.000Z",
                "amount": "207500.70",
                "reference": "hasil jualan pulsa",
                "created_at": "2021-07-23T08:13:06.000Z"
            }
        ]
    }

GET /finance/:id -> show where soft_delete is null
    params: id, headers: authtoken
    return: data of finance by id
    {
        "id": 7,
        "transaction_time": "2021-07-22T23:30:02.000Z",
        "amount": "15000.00",
        "reference": "nasi padang pinjaman",
        "created_at": "2021-07-23T08:13:47.000Z"
    }


POST /finance
    params: amount, reference, transaction_time, user_id, finance_type_id, headers: authtoken
    return: data of created
    {
        "id": 9,
        "amount": 337500.75,
        "reference": "hasil jualan",
        "user_id": 105,
        "finance_type_id": 1,
        "transaction_time": "2021-07-22T23:30:02.000Z",
        "created_at": "2021-07-23T08:20:29.840Z",
        "updated_at": "2021-07-23T08:20:29.843Z"
    }

PUT /finance/:id
    params: id, amount, reference, transaction_time, finance_type_id, headers: authtoken
    return: 1 if updated success

DELETE /finance/:id
    params: id, headers: authtoken
    return: 1 if soft deleted success
```

